# Dbgscrpt



## ¿Qué es dbgscrpt?

dbgscrpt es una utilidad para crear un script que puede ser interpretado por el programa debug de windows de la siguiente forma: debug < archivo. Esta pequeña utilería es funcional y esta archivada ya que no se realizarán mas cambios o actualizaciones.

Se libera bajo licencia de software libre (aunque es un programa muy pequeño, pero para evitar malos entendidos).
