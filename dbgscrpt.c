/*
	dbgscrpt v1.0
	JUNIO 2010

	ESTA UTILIDAD CREA EL SCRIPT DE UN ARCHIVO PARA
	DESPUES SER INTERPRETADO CON EL DEBUG DE WINDOWS DE LA
	SIGUIENTE FORMA: DEBUG < ARCHIVO

	Copyright (C) 2010 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

#include <stdio.h>
int main(int argc,char *argv[])
{
    unsigned int i=0X00,j=0;
    unsigned char x;    //si es un char signado nos da cosas como 0XFFF92
    FILE    *origen,*destino;
        if(argc==1){    //si estamos aqui significa que no hay parametros, dar informacion
            printf("dbgscrpt por J. Nieto kkeezzff@hotmail.com\n");
		printf("dbgscrpt  Copyright (C) 2010  J. Nieto\n");
    		printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
    		printf("This is free software, and you are welcome to redistribute it\n");
		printf("under certain conditions.\n");
            printf("dbgscrpt [archivo origen] [archivo destino]\n");
        }else{
            origen=fopen(argv[1],"rb"); //abriendo el archivo en modo binario para lectura
            destino=fopen(argv[2],"w"); //habirendo el archivo para escritura
                if (origen!=NULL && destino!=NULL){
                    fprintf(destino,"N %s\n",argv[1]);  //imprime primer linea en archivo destino
                    fprintf(destino,"E %.4X ",i+0X100); //imprime el inicio de la segunda linea en archivo destino
                    fscanf(origen,"%c",&x); //leemos el primer caracter del archivo
                        while(!feof(origen)){   //mientras no alcanemos el EOF seguimos en el loop
                            fprintf(destino,"%.2X ",x); //imprimirmos el hexadecimal al archivo destino
                            fscanf(origen,"%c",&x); //leemos el caracter del archivo origen
                            i++;    //variable que contiene cuantos bits a escribir
                            j++;    //variable que cuenta hasta 16 para dar el formato al archivo
                            if(j==16){
                                fprintf(destino,"\n");  //si es j=16 da un retorno de carro
                                j=0;    //resetea j
                                fprintf(destino,"E %.4X ",i+0X100); //imprime nuevo inicio de linea en archivo destino
                            }
                        }
                    fprintf(destino,"\nRCX\n%.4X\nW\nQ\n",i);   //imprime ultimas lineas en el archivo destino
                    fclose(origen); //cierra archivo origen
                    fclose(destino);    //cierra archivo destino
                }
        }
    return 0;
}
